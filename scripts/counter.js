$(document).ready(function(){
        
  var val = 6400,
      valMin = val,       
      valStep = 2400,
      valMax = 100000,
      counter = $('.counter__val').text('от ' + val + ' c'),
      counterMin = $('.controls__btn_min'),
      counterMax = $('.controls__btn_max'),
      cashStep = 1,
      cashBox = $('.counter__cashbox').text(cashStep + ' касса');
  
  counterMax.on('click', function(e){
    e.preventDefault();
    val = val + valStep;
    counter = counter.text('от ' + val + ' c')
    setValue(valMin, valMax)
    cashBox = cashBox.text(cashStep++ + ' касса');
    if(cashStep > 39){
      cashStep = 39;
      
    }
  });

  counterMin.on('click', function(e){
    e.preventDefault();
    val = val - valStep;
    counter = counter.text('от ' + val + ' c');
    setValue(valMin, valMax)
    cashBox = cashBox.text(cashStep-- + ' касса')
    if (cashStep < 1) {
      cashStep = 1;
    }
  });

  function setValue(min, max){
    if (val >= max) {
      counter.text('от ' + max + ' c')

    } else if (val <= min) {
      counter.text('от ' + min + ' c')
    }
  }
});