$(function () {
  var items = $('.slider__item');
  
  $("#slider").slider({
    value: 100,
    min: 50000,
    max: 100000,
    step: 2500,
    slide: function (event, ui) {
      $("#amount").val(ui.value + " / месяц");

      items.each(function(i){
        var item = items[i];
  
        if (item.getAttribute('data-val') <= ui.value){
          item.className = 'slider__item active';
        }else{
          item.className = 'slider__item';
        }
      });
     
    }
  });

  $("#amount").val($("#slider").slider("value") + " / месяц");
});